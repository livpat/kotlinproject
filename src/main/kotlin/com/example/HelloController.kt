package com.example

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces

@Controller
class HelloController {

    @Get("/echo/{s}")
    fun echo(s: String): String {
        return s
    }
}