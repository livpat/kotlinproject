package com.example
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class HelloWorldTest {

    @Inject
    @field:Client("/")
    lateinit var client : RxHttpClient

    @Test
    fun testHello() {
        val foo = client.toBlocking().retrieve(HttpRequest.GET<Any>("/echo/foo"))
        Assertions.assertNotNull(foo)
        Assertions.assertEquals("foo", foo)
    }

}
